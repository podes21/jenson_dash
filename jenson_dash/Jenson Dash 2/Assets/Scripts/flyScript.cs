﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyScript : MonoBehaviour {

	public float flyingForce = 65.0f;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		bool flyingYes = Input.GetButton ("Jump");

		if (flyingYes) {
			rb.AddForce(new Vector2 (0, flyingForce));
		}
	}
}
